# Effect of population heterogeneity in disease dynamics
### A project for Computational Epidemiology (Course: 467294-HS2021)

[![Streamlit](https://static.streamlit.io/badges/streamlit_badge_black_white.svg)](https://share.streamlit.io/noahhenrikkleinschmidt/computational_epidemiology_sir_model/main/main.py)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/NoahHenrikKleinschmidt%2FComputation_Epidemiology_HS21/main?labpath=modelling.ipynb)

This project models the effect of a variety of subgroups within a population on disease dynamics. To that end an SIRD model was developed that models a susceptible population of various subgroups that differ in their infection rate, recovery rate, death rate, and/or relapsation rate (loss of immunity). Four different populations are assumed to that end: Susceptible $`S`$, Infectuous $`I`$, Recovered $`R`$, and Deceased $`D`$. Colloquially, the term "population" will be used to refer to any or all non-deceased populations.

This project includes an interactive webapp. It is accessible via the `Streamlit` Badge above.
Example Modellings can be inspected in the Binder Notebook accessible via the `Binder` Badge above. 


## The SIRD Model
---

This model assumes the population contains a reference subgroup termed $`S_n`$ (for "normally" susceptibles) that transitions between Susceptible population $`S`$, to Infectuous $`I`$, Recovered $`R`$, or Desceased $`D`$ with some default transition rates (see below). This model allows for an arbitrary number of divergent subgroups $`S_i`$ within the population $`S`$, represented by a percentage $`\alpha_i`$. 

Consequently, it holds that $`\alpha_n + \sum \alpha_i = 1`$, where $`\alpha_n`$ is the percentage of the reference subgroup within $`S`$. And the total population is thus given by:

```math
S = S_n + \sum S_i = \alpha_n \cdot S + \sum (\alpha_i  \cdot S) = (\alpha_n + \sum \alpha_i)\cdot S
```

### Divergent Subgroups

For the purpose of this model each divergent subgroup is modelled as a linear-transform of the reference group. Hence, each $`S_i`$ is distinguished by a scalar factor $`x`$ for each transition rate. As this model considers four transition rates (see below) each $`S_i`$ will be associated with a tuple of four corresponding $`x_t \in \mathbb{R}`$, one for each transition $`t`$. The impact of each divergent subgroup is given by its percentual representation within the overall population. Consequently, we can define a weight function $`\Phi_t`$ that assignes the impact of each divergent subgroup for a given transition $`t`$

```math
\Phi_t = \alpha_n + \sum_{i}(x_{i,t}\cdot \alpha_i)
```

where $`x_{i,t}`$ is the respective scalar factor for a given transition $`t`$ of subgroup $`i`$. 
The actual transition rate for the overall system $`\lambda_t`$ is hence given by

```math
\lambda_t = \Phi_t \cdot t
```

where $`t \in \mathbb{R}`$ denotes the default transition rate. 


### Transitions
This model allows for four transitions between the different populations. 
(1) Susceptibles may transition into Infectuous with a default infection rate $`\beta`$. 
(2) Infectuous may transition into Recovered with a default recovery rate $`\gamma`$. 
(3) Alternatively, Infectuous may transition into Deceased with a default death rate $`\theta`$. 
(4) Recovered may transition back into Susceptibles when they loose their immunity over time with a default relapsation rate $`\delta`$. 


### System of Equations 

The outlined model allows the following set of first order differential equations to be formulated:

```math
\begin{align}
\frac{dS(t)}{dt} = {}&  -\lambda_\beta \cdot S(t) \cdot I(t) +\lambda_\delta \cdot R(t) \\

\frac{dI(t)}{dt} = {}&  \lambda_\beta \cdot S(t) \cdot I(t) - (\lambda_\gamma + \lambda_\theta )\cdot I(t) \\

\frac{dR(t)}{dt} = {}& \lambda_\gamma \cdot I(t) - \lambda_\delta \cdot R(t) \\

\frac{dD(t)}{dt} = {}&  \lambda_\theta \cdot I(t)
\end{align}
```


